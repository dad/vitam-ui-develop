//Seda element constants :
//Seda elelemtns can be attributes, simple or complex elements

export enum SedaElementConstants {
    attribute = 'Attribute',
    simple = 'Simple',
    complex = 'Complex'
}

//Seda choice constants : can be yes or no
export enum SedaChoiceConstants {
    yes = 'yes',
    no = 'no',
}

//Seda Extensible constants : can be yes or no
export enum SedaExtensibleConstants {
    yes = 'yes',
    no = 'no',
}


export enum SedaCardinalityConstants {
    'zeroOrOne' = '0-1',
    'one' = '1',
    'oreOrMore' = '1-N',
    'zeroOrMore'  = '0-N'
}


export enum SedaCollections {
    'object' = 'Objets',
    'header' = 'Entete',
    'rules' = 'Regles',
    'arborescent'  = 'Aborescence'
}

export interface SedaData {
    Name:string;
    Type:string;
    Element:string;
    Cardinality:string;
    Definition:string;
    Extensible:boolean;
    Choice:boolean;
    Children: SedaData[];
    Enumeration:string[];
    Collection: SedaCollections;
}
