import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FileUploadModule } from 'ng2-file-upload';
import { MatIconModule,MatButtonModule,MatDialogModule,} from '@angular/material'
import { UserActionUploadProfileComponent } from './user-action-upload-profile/user-action-upload.component';
import { SharedModule} from '../shared/shared.module'
import { FilterByNamePipe } from './user-action-add-metadata/user-action-add-metadata.component';
import { UserActionSaveProfileComponent } from './user-action-open-profile/user-action-save-profile.component';
import { UserActionsComponent } from './user-actions.component';


@NgModule({
  declarations: [UserActionUploadProfileComponent,FilterByNamePipe, UserActionSaveProfileComponent,UserActionsComponent],
  imports: [
    CommonModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    FileUploadModule,
    SharedModule
  ],
  exports: [UserActionUploadProfileComponent, UserActionSaveProfileComponent,FilterByNamePipe ],
})
export class UserActionsModule {

 }
