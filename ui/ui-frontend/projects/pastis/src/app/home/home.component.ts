import {Component,OnInit,ViewChild} from '@angular/core';
import { CdkTextareaAutosize} from '@angular/cdk/text-field';
import {  Router,  ActivatedRoute} from '@angular/router';
import {  ToggleSidenavService} from '../core/services/toggle-sidenav.service';
import {  RegisterIconsService} from '../core/services/register-icons.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  search = '';
  tenantId: number;

  @ViewChild('treeSelector', {static: true}) tree: any;
  @ViewChild('autosize', {static: false}) autosize: CdkTextareaAutosize;


  opened: boolean;
  events: string[] = [];

  constructor(private sideNavService: ToggleSidenavService,
    private iconService: RegisterIconsService,
    private route: ActivatedRoute,
    private router: Router) {

    this.sideNavService.isOpened.subscribe(status => {
      this.opened = status;
    })

    this.route.params.subscribe(params => {
      if (params.tenantIdentifier) {
        this.tenantId = params.tenantIdentifier;
      }
    });
  }

  ngOnInit() {
    this.iconService.registerIcons()
  }

  openSideNav() {
    this.opened = true;
  }


  closeSideNav() {
    this.opened = false;
  }
  changeTenant(tenantIdentifier: number) {
    this.tenantId = tenantIdentifier;
    this.router.navigate(['..', tenantIdentifier], {
      relativeTo: this.route
    });
  }
}
