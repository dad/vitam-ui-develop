/*
 * Copyright CINES Centre Informatique National de l'Enseignement Supérieur, 2017
 * Tout droit d'utilisation strictement soumis à l'approbation du CINES
 */
package fr.gouv.vitamui.pastis.model.jaxb;

import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlValue;

/**
 * @author rachid Sala <rachid@cines.fr>
 */
public class DocumentationXML { 
    
    String documentation;
    
    @XmlValue
    public String getDocumentation() {
        return documentation;
    }

    public void setDocumentation(String documentation) {
        this.documentation = documentation;
    }

    BaliseXML parent;
    
    @XmlTransient
    public BaliseXML getParent() {
        return parent;
    }

    public void setParent(BaliseXML parent) {
        this.parent = parent;
    }
}