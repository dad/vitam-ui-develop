/*
 * Copyright CINES Centre Informatique National de l'Enseignement Supérieur, 2017
 * Tout droit d'utilisation strictement soumis à l'approbation du CINES
 */
package fr.gouv.vitamui.pastis.util;

import fr.gouv.vitamui.pastis.model.ElementRNG;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.Stack;


/**
 * @author Paulo Pimenta <pimenta@cines.fr>
 */
public class PastisSAX2Handler extends DefaultHandler{

	private static Logger LOGGER = LoggerFactory.getLogger(PastisSAX2Handler.class);

	public String jsonParsed = "";

	public int oldSize = 0;

	boolean isValue = false;

	private boolean isInDocumentationTag = false;

	public ElementRNG elementRNGRoot;

	Stack<ElementRNG> stackRNG = new Stack<ElementRNG>();

	private StringBuilder documentationContent;


	/**
	 BEGIN OF OVERRIDE OF SAX 5 METHODS : startElement, endElement, startDocument, endDocument and characters
	 * This method is called everytime the parser gets an open tag
	 * Identifies which tag has being opened at time by assiging a new flag
	 */
	public void startElement(String nameSpace, String localName, String qName, Attributes attr) throws SAXException {

		//cette variable contient le nom du nœud qui a créé l'événement
		// If node not a grammar tag or start tag
		if(!("grammar".equals(localName) || "start".equals(localName))){

			// If node is ArchiveTransfer
			if(null !=attr.getValue("name") && attr.getValue("name").equals("ArchiveTransfer")){
				return ;
			}
			//If node has documentation
			if("value".equals(localName) || "documentation".equals(localName)) {
				this.isValue = true;
			}
			// Create a new rng tag element and add it to the stack
			ElementRNG elementRNG = new ElementRNG();
			elementRNG.setName(attr.getValue("name"));
			elementRNG.setType(localName);
			elementRNG.setDataType(attr.getValue("type"));
			if(!stackRNG.empty()) {
				ElementRNG e = stackRNG.lastElement();
				elementRNG.setParent(e);
				e.getChildren().add(elementRNG);
			}
			stackRNG.push(elementRNG);



		}

		documentationContent = new StringBuilder();
		if (qName.equalsIgnoreCase("xsd:documentation")) {
			isInDocumentationTag = true;
		}

	}

	/**
	 * Actions à réaliser lors de la détection de la fin d'un élément.
	 */
	public void endElement(String nameSpace, String localName, String qName) throws SAXException {

		if (qName.equalsIgnoreCase("xsd:documentation")) {
			isInDocumentationTag = false;

		}
		if(!stackRNG.empty()) {
			ElementRNG e = stackRNG.pop();
		}
	}

	/**
	 * Actions à réaliser au début du document.
	 */
	public void startDocument() {
		elementRNGRoot = new ElementRNG();
		elementRNGRoot.setName("ArchiveTransfer");
		elementRNGRoot.setType("element");
		stackRNG.push(elementRNGRoot);
	}

	/**
	 * Actions à réaliser lors de la fin du document XML.
	 */
	public void endDocument() {
	}

	/**
	 * Actions to perform when tag content is reached (Data between '< />' )
	 */
	@Override
	public void characters(char[] caracteres, int start, int length) throws SAXException {
		if (isInDocumentationTag) {
			documentationContent.append(new String(caracteres, start, length));
			stackRNG.lastElement().setValue(documentationContent.toString());
		}
		if(isValue) {
			String valueContent = new String(caracteres, start, length);
			stackRNG.lastElement().setValue(valueContent);
			this.isValue = false;
		}
	}
}
