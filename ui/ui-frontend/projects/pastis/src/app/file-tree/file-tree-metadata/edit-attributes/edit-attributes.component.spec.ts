import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAttributesPopupComponent } from './edit-attributes.component';

describe('EditAttributesPopupComponent', () => {
  let component: EditAttributesPopupComponent;
  let fixture: ComponentFixture<EditAttributesPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditAttributesPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAttributesPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
