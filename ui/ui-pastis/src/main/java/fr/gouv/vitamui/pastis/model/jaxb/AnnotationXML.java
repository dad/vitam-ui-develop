/*
 * Copyright CINES Centre Informatique National de l'Enseignement Supérieur, 2017
 * Tout droit d'utilisation strictement soumis à l'approbation du CINES
 */
package fr.gouv.vitamui.pastis.model.jaxb;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author rachid Sala <rachid@cines.fr>
 */
@XmlRootElement (name="xsd:annotation")
public class AnnotationXML extends BaliseXML{
    
    DocumentationXML documentationXML;

    @XmlElement (name="xsd:documentation")
    public DocumentationXML getDocumentationXML() {
        return documentationXML;
    }

    public void setDocumentationXML(DocumentationXML documentationXML) {
        this.documentationXML = documentationXML;
    } 
    
}