import{IEnvironment} from './IEnvironment'

export const environment: IEnvironment= {
  production: false,
  apiPastisUrl: 'https://dev.vitamui.com:9051',
  apiOntologyUrl: 'http://localhost:8080',
  name: 'dev'
};
