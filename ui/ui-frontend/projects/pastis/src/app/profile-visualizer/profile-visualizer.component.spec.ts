import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileVisualizerComponent } from './profile-visualizer.component';

describe('SedaVisualizerComponent', () => {
  let component: ProfileVisualizerComponent;
  let fixture: ComponentFixture<ProfileVisualizerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileVisualizerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileVisualizerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
