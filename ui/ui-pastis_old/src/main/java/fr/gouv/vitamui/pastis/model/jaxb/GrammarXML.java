package fr.gouv.vitamui.pastis.model.jaxb;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement (name="rng:grammar")
public class GrammarXML extends BaliseXML {

	StartXML start;

    @XmlAttribute(name = "datatypeLibrary")
    private String datatypeLibrary = "http://www.w3.org/2001/XMLSchema-datatypes";

    @XmlAttribute(name = "ns")
    private String ns = "fr:gouv:culture:archivesdefrance:seda:v2.1";

    @XmlAttribute(name = "xmlns")
    private String xmlns = "fr:gouv:culture:archivesdefrance:seda:v2.1";

    @XmlAttribute(name = "xmlns:xsd")
    private String xd = "http://www.w3.org/2001/XMLSchema";

	public StartXML getStart() {
		return start;
	}

	public void setStart(StartXML start) {
		this.start = start;
	}

}
