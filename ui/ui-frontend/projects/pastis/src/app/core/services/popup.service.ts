import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PopupService {

  popUpDataBeforeClose = new BehaviorSubject<any>(null);
  btnYesShoudBeDisabled= new BehaviorSubject<boolean>(false);


  constructor() { }

  getPopUpDataOnClose(){
    return this.popUpDataBeforeClose;
  }
  setPopUpDataOnClose(incomingData:any){
    this.popUpDataBeforeClose.next(incomingData);
  }
  disableYesButton(condition:boolean){
    condition ? this.btnYesShoudBeDisabled.next(true) : this.btnYesShoudBeDisabled.next(false);
  }


}
