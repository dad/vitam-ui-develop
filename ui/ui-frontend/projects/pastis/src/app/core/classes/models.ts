export interface CardinalityValues {
    value: string;
    viewValue: string;
  }

export interface MetadataHeaders {
    id: number;
    nomDuChamp: string;
    type: string;
    valeurFixe: string;
    cardinalite: string[];
    commentaire: string;
    enumeration: string[];
  }
