import {registerLocaleData} from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import {LOCALE_ID, NgModule} from '@angular/core';
import {MatNativeDateModule} from '@angular/material';
import {BrowserModule, Title} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgxFilesizeModule} from 'ngx-filesize';
import {VitamUICommonModule, WINDOW_LOCATION, BASE_URL, ENVIRONMENT} from 'ui-frontend-common';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {CoreModule} from './core/core.module';
import {SharedModule} from './shared/shared.module';
import {HomeModule} from './home/home.module';

import { environment } from 'projects/demo/src/environments/environment';

registerLocaleData(localeFr, 'fr');

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    CoreModule,
    BrowserAnimationsModule,
    BrowserModule,
    VitamUICommonModule,
    AppRoutingModule,
    MatNativeDateModule,
    NgxFilesizeModule,
    SharedModule,
    HomeModule
  ],
  providers: [
    Title,
    { provide: BASE_URL, useValue: '/portal-api' },
    { provide: ENVIRONMENT, useValue: environment },
    { provide: LOCALE_ID, useValue: 'fr'},
    { provide: WINDOW_LOCATION, useValue: window.location}
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
