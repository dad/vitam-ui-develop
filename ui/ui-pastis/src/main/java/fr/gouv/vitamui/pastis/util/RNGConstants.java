/*
 * Copyright CINES Centre Informatique National de l'Enseignement Supérieur, 2017
 * Tout droit d'utilisation strictement soumis à l'approbation du CINES
 */
package fr.gouv.vitamui.pastis.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @author rachid Sala <rachid@cines.fr>
 */
public class RNGConstants { 
    
    public static enum DataType
    {
        anyURI("anyURI"),
        token("token"),
        string("string"),
        dateTime("dateTime"),
        ID("ID");        
        private String label;

        private DataType(final String value) {
            setLabel(value);
        }
        public String getLabel() {
            return label;
        }
        public void setLabel(final String label) {
            this.label = label;
        }
    }
    
    public static enum MetadaDataType
    {
        element ("element"),
        attribute("attribute"),
        data("data"),
        except("except"),
        nsName("nsName"),
        value("value"),
        text("text"),
        ID("ID");        
        private String label;

        private MetadaDataType(final String value) {
            setLabel(value);
        }
        public String getLabel() {
            return label;
        }
        public void setLabel(final String label) {
            this.label = label;
        }
    }
    
    public static final Map<String , String> CardinalityMap = new HashMap<String , String>() {
        public static final long serialVersionUID = 1L;
    {
        put("optional",    "0-1");
        put("zeroOrMore", "0-N");
        put("obligatoire",   "1");  
        put("oneOrMore",   "1-N");
    }};
    
    public static enum Cardinality
    {
        optional("0-1"),
        zeroOrMore("0-N"),
        obligatoire("1"),
        oneOrMore("1-N");        
        private String label;

        private Cardinality(final String value) {
            setLabel(value);
        }
        public String getLabel() {
            return label;
        }
        public void setLabel(final String label) {
            this.label = label;
        }
    }
    
    public static final Map<String , String> GroupOrChoiceMap = new HashMap<String , String>() {
        public static final long serialVersionUID = 1L;
    {
        put("group","group");
        put("choice","choice");
    }};
    
    public static enum GroupOrChoice
    {
        group("group"),
    	choice("choice");
        private String label;
        
        private GroupOrChoice(final String value) {
            setLabel(value);
        }
        public String getLabel() {
            return label;
        }
        public void setLabel(final String label) {
            this.label = label;
        }
    }
    
    
    public static boolean isElement (String type) {
        for(MetadaDataType typeElement : RNGConstants.MetadaDataType.values()){
            if (typeElement.toString().equals(type)) {
                return true;
            }
        }
        return false;
    }
    
    public static boolean isCardinality (String type) {
        for(Cardinality typeElement : RNGConstants.Cardinality.values()){
            if (typeElement.toString().equals(type)) {
                return true;
            }
        }
        return false;
    }
    
    public static boolean hasGroupOrChoice(String type) {
        for(GroupOrChoice typeElement : RNGConstants.GroupOrChoice.values()){
            if (typeElement.toString().equals(type)) {
                return true;
            }
        }
        return false;
    }
    
    public static boolean isDataType (String type) {
        for(DataType typeElement : RNGConstants.DataType.values()){
            if (typeElement.toString().equals(type)) {
                return true;
            }
        }
        return false;
    }
    
    public static boolean isValueOrData (String type) {
        
            if (null != type && (RNGConstants.MetadaDataType.data.toString().equals(type)
            		|| RNGConstants.MetadaDataType.nsName.toString().equals(type)
                    || RNGConstants.MetadaDataType.value.toString().equals(type))){
                return true;
            }
        return false;
    }
}