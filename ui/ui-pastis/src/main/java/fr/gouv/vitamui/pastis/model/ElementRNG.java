/*
 * Copyright CINES Centre Informatique National de l'Enseignement Supérieur, 2017
 * Tout droit d'utilisation strictement soumis à l'approbation du CINES
 */
package fr.gouv.vitamui.pastis.model;

import fr.gouv.vitamui.pastis.util.RNGConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Paulo Pimenta <pimenta@cines.fr>
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ElementRNG { 
    
    public ElementRNG() {
    }
    
    String name;

    String type;
    
    String dataType;   
    
    String value;
    
    ElementRNG parent;

    private static Logger LOGGER = LoggerFactory.getLogger(ElementRNG.class);


    public static ElementProperties elementStatic = new ElementProperties();
    
    public static ElementProperties elementStaticRoot = new ElementProperties();
    
    private static long idCounter = 0;
    
    List<ElementRNG> children = new ArrayList<ElementRNG>();    
    
    @XmlAttribute  
    public String getName() {
        return name;
    }

    public void setName(String name) {        
        this.name = name;
    }

    @XmlElement
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @XmlAttribute (name="type")
    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    @XmlElement(name="rng:value")
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @XmlTransient
    public ElementRNG getParent() {
        return parent;
    }

    public void setParent(ElementRNG parent) {
        this.parent = parent;
    }

    @XmlAnyElement
    public List<ElementRNG> getChildren() {
        return children;
    }

    public void setChildren(List<ElementRNG> children) {
        this.children = children;
    }   
    
    public static void setDataForParentElementOrAttribute (ElementProperties parentNode, ElementRNG node) {
        if(null != parentNode.getType() && (RNGConstants.MetadaDataType.element.toString().equals(parentNode.getType())
        		|| RNGConstants.MetadaDataType.attribute.toString().equals(parentNode.getType()))) {
            parentNode.setValueOrData(node.getType());
            parentNode.setDataType(node.getDataType());
            parentNode.setValue(node.getValue());
        }else {
            setDataForParentElementOrAttribute(parentNode.getParent(), node);
        }
    }
    
    
    public static void setDocumentationForParentElement (ElementProperties parentNode, ElementRNG node) {
        if(null != parentNode.getType() && RNGConstants.MetadaDataType.element.toString().equals(parentNode.getType())) {
            parentNode.setDocumentation(node.getValue());
        }else {
        	setDocumentationForParentElement(parentNode.getParent(), node);
        }
    }
    
    public static void setElementsForGroupOrChoice(ElementProperties parentNode, ElementRNG node) {
        
    	if(null != parentNode.getType() && (RNGConstants.GroupOrChoice.group.toString().equals(parentNode.getType()) 
        		|| RNGConstants.GroupOrChoice.choice.toString().equals(parentNode.getType()))) {
            parentNode.setGroupOrChoice(node.getType());
        }else {
        	setElementsForGroupOrChoice(parentNode.getParent(), node);
        }
    }
    // Build the a tree of properties given :
    // a node
    //the level of the node 
    //the parent of the node
    public static ElementProperties buildElementPropertiesTree( ElementRNG node, int profondeur, ElementProperties parentNode ){
        ElementProperties local = new ElementProperties();
        LOGGER.info("Generating JSON element {}", node.getName());
        if(null != node.getType() && RNGConstants.MetadaDataType.element.toString().equals(node.getType())
        		|| RNGConstants.MetadaDataType.attribute.toString().equals(node.getType())) {
            
			local.setCardinality(elementStatic.getCardinality());
        	local.setGroupOrChoice(elementStatic.getGroupOrChoice());
            local.setName(node.getName());
            local.setType(node.getType());
            local.setLevel(profondeur);
            local.setValue(node.getValue());


            elementStatic = new ElementProperties();
        
            if(null != parentNode) {
                local.setParent(parentNode);
                local.setParentId(parentNode.getId());
                local.setId(ElementRNG.idCounter++);
                parentNode.getChildren().add(local);
            }else {
                local.setId(ElementRNG.idCounter++);
                local.setParentId(null);
                elementStaticRoot = local;
            }
        }
        
        else {

            if(RNGConstants.isValueOrData(node.getType())) {
                setDataForParentElementOrAttribute(parentNode, node);
            }else if(RNGConstants.isCardinality(node.getType())) {  
                elementStatic.setCardinality(node.getType());
            }else if(RNGConstants.hasGroupOrChoice(node.getType())) {  
                elementStatic.setGroupOrChoice(node.getType());
            }
            else if("documentation".equals(node.getType())) {
                if(null != node.getValue()) {
                    setDocumentationForParentElement(parentNode, node);
                }
            }

            local = parentNode;
        }
 
      for( ElementRNG next : node.getChildren() ) {
          if(null != next.getType() && (RNGConstants.MetadaDataType.element.toString().equals(next.getType())
        		  || RNGConstants.MetadaDataType.attribute.toString().equals(next.getType()))) {
              buildElementPropertiesTree( next, profondeur + 1, local );              
          }else {
              buildElementPropertiesTree( next, profondeur, local );
          }
      }
      return local;
    }
}
