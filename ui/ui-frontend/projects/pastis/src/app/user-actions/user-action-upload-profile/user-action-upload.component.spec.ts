import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserActionUploadProfileComponent } from './user-action-upload.component';

describe('UserActionUploadComponent', () => {
  let component: UserActionUploadProfileComponent;
  let fixture: ComponentFixture<UserActionUploadProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserActionUploadProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserActionUploadProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
